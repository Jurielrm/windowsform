﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Programa02_04
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnForma2_Click(object sender, EventArgs e)
        {
            //Crea la instancia de la segunda forma
            Form2 miForma2 = new Form2();
            //Se musetra el resultado
            DialogResult resultado = miForma2.ShowDialog();
            //Desplega la instancia
            miForma2.ShowDialog();
            //Si el usuario selecciono el boton OK llevara la actulizacion de las etiquetas  
            if (resultado == DialogResult.OK)
            {
                //Obtiene el contenido de la propiedad de mensaje
                lblMensaje.Text = miForma2.Mensaje;
                //Actualizamos el mensaje de contenido
                lblContenido.Text = miForma2.Contenido;
            }
            if (resultado == DialogResult.Cancel)
            {
                MessageBox.Show("No ejecutaste la operacion");
            }
        }
    }
}
