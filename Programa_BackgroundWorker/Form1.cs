﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Programa_BackgroundWorker
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            DateTime Inicio = DateTime.Now; //Se Define el inicio de la tarea
            e.Result = "";
            for (int i = 1; i <= 100; i++)
            {
                System.Threading.Thread.Sleep(1000); //Sumamos el trabajo que debe realizar el bacgorundWorkwe
                backgroundWorker1.ReportProgress(i, DateTime.Now); //Complemtamos un porcentaje del trabajo y lo notificamos 

                if (backgroundWorker1.CancellationPending)
                {
                    e.Cancel = true;
                    return;
                }
            }
            TimeSpan duracion = DateTime.Now - Inicio; //Calcular el intervalo de venta que le llevo a realizar la tarea
            e.Result = "Duracion: " + duracion.TotalMinutes.ToString() + "Minutos"; //Imprime el tiempo de ejecucion
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            PgBTarea.Value = e.ProgressPercentage; //Actualizar la barra de tarea
            lblPorcentaje.Text = Convert.ToString(PgBTarea.Value) + "%";
            DateTime Tiempo = Convert.ToDateTime(e.UserState);

            txtSalida.AppendText(Tiempo.ToLongTimeString());
            txtSalida.AppendText(Environment.NewLine);
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                MessageBox.Show("La tarea ha sido cancelada");
            }
            else
            {
                MessageBox.Show("La tarea ha sido completado con exito. Resultados: " + e.Result.ToString());
            }
                  
        }

        private void btnInicio_Click(object sender, EventArgs e)
        {
            backgroundWorker1.RunWorkerAsync();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            backgroundWorker1.CancelAsync();
        }
    }
}
