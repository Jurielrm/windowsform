﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Programa
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            chlbAlimentos.Items.Add("Carne");
            chlbAlimentos.Items.Add("Pescado", true);
            lblNombre.Text = "";
        }

        private void btnAdicionar_Click(object sender, EventArgs e)
        {
            //Adiciona mas checkbox en el menu
            chlbAlimentos.Items.Add(txtAlimentos.Text);
        }

        private void chlbAlimentos_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Se guarda el numero de indice que este selecionado
            int indice = chlbAlimentos.SelectedIndex;
            
            if (indice != -1)
            {
                lblNombre.Text = chlbAlimentos.Items[indice].ToString();         
            }
            //Cuando se selecione el alimento pescado, mande un mensaje 
            if (chlbAlimentos.GetItemChecked(3) == true)
                MessageBox.Show("El pescado es bueno");

        }
    }
}
