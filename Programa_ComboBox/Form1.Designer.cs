﻿namespace Programa_ComboBox
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmbConsolas = new System.Windows.Forms.ComboBox();
            this.txtConsola = new System.Windows.Forms.TextBox();
            this.btnAdicionar = new System.Windows.Forms.Button();
            this.lblIndice = new System.Windows.Forms.Label();
            this.lblText = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // cmbConsolas
            // 
            this.cmbConsolas.FormattingEnabled = true;
            this.cmbConsolas.Items.AddRange(new object[] {
            "Xbox",
            "PlayStation"});
            this.cmbConsolas.Location = new System.Drawing.Point(119, 34);
            this.cmbConsolas.Name = "cmbConsolas";
            this.cmbConsolas.Size = new System.Drawing.Size(158, 21);
            this.cmbConsolas.TabIndex = 0;
            this.cmbConsolas.Text = "Consolas";
            this.cmbConsolas.SelectedIndexChanged += new System.EventHandler(this.cmbConsolas_SelectedIndexChanged);
            // 
            // txtConsola
            // 
            this.txtConsola.Location = new System.Drawing.Point(84, 141);
            this.txtConsola.Name = "txtConsola";
            this.txtConsola.Size = new System.Drawing.Size(100, 20);
            this.txtConsola.TabIndex = 1;
            // 
            // btnAdicionar
            // 
            this.btnAdicionar.Location = new System.Drawing.Point(244, 139);
            this.btnAdicionar.Name = "btnAdicionar";
            this.btnAdicionar.Size = new System.Drawing.Size(75, 23);
            this.btnAdicionar.TabIndex = 2;
            this.btnAdicionar.Text = "Adicionar";
            this.btnAdicionar.UseVisualStyleBackColor = true;
            this.btnAdicionar.Click += new System.EventHandler(this.btnAdicionar_Click);
            // 
            // lblIndice
            // 
            this.lblIndice.AutoSize = true;
            this.lblIndice.Location = new System.Drawing.Point(81, 92);
            this.lblIndice.Name = "lblIndice";
            this.lblIndice.Size = new System.Drawing.Size(35, 13);
            this.lblIndice.TabIndex = 3;
            this.lblIndice.Text = "label1";
            // 
            // lblText
            // 
            this.lblText.AutoSize = true;
            this.lblText.Location = new System.Drawing.Point(259, 92);
            this.lblText.Name = "lblText";
            this.lblText.Size = new System.Drawing.Size(35, 13);
            this.lblText.TabIndex = 4;
            this.lblText.Text = "label2";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(405, 348);
            this.Controls.Add(this.lblText);
            this.Controls.Add(this.lblIndice);
            this.Controls.Add(this.btnAdicionar);
            this.Controls.Add(this.txtConsola);
            this.Controls.Add(this.cmbConsolas);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbConsolas;
        private System.Windows.Forms.TextBox txtConsola;
        private System.Windows.Forms.Button btnAdicionar;
        private System.Windows.Forms.Label lblIndice;
        private System.Windows.Forms.Label lblText;
    }
}

