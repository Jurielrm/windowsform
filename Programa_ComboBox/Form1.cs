﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Programa_ComboBox
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            cmbConsolas.Items.Add("Wii");
            cmbConsolas.Items.Add("Nintendo");
            lblIndice.Text = "";
            lblText.Text = "";  
        }

        private void btnAdicionar_Click(object sender, EventArgs e)
        {
            cmbConsolas.Items.Add(txtConsola.Text);

        }

        private void cmbConsolas_SelectedIndexChanged(object sender, EventArgs e)
        {
            int indice = cmbConsolas.SelectedIndex;
            lblIndice.Text = indice.ToString();
            lblText.Text = cmbConsolas.Items[indice].ToString();
        }
    }
}
